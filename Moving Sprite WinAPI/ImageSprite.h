#pragma once

#include "Sprite.h"

class ImageSprite :
	public Sprite
{
public:
	ImageSprite(RECT rect);
	ImageSprite(int left, int top, int right, int bottom);
	~ImageSprite();

	virtual void Draw(HDC hdc) override;
	virtual void Move(int dx, int dy) override;
	virtual bool Hit(POINT hdc) override;
private:
	HRGN region;
	HANDLE image;
};

