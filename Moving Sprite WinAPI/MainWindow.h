#pragma once

#include <windows.h>

class Sprite;

class MainWindow
{
public:
	~MainWindow();

	static ATOM RegisterWindowClass(HINSTANCE hInstance);
	static MainWindow* CreateNewWindow(LPCWSTR className, HWND parent);

	HWND GetHWND();
private:
	void ReleaseResources();
	void ProtectBorders();

	static LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT OnMouseWheel(UINT uMsg, WPARAM wParam, LPARAM lParam);

	LRESULT OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT OnMouseLeave(UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam);

	
	void OnUpdateTimer();
	
	MainWindow(HWND hwnd);

	POINT lastDraggingPoint;
	bool isDragging;
	HWND hwnd;
	Sprite* sprite;
};
