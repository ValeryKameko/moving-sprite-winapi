#include "HInstance.h"

HINSTANCE HInstance::hInstance = NULL;

HINSTANCE HInstance::GetHInstance()
{
	return hInstance;
}

void HInstance::SetHInstance(HINSTANCE hInstance)
{
	HInstance::hInstance = hInstance;
}

HInstance::HInstance()
{
}
