#include "RectangleSprite.h"



RectangleSprite::RectangleSprite(RECT rect)
	: Sprite(rect)
{
}

RectangleSprite::RectangleSprite(int left, int top, int right, int bottom)
	: Sprite(left, top, right, bottom)
{
}

RectangleSprite::~RectangleSprite()
{
}

void RectangleSprite::Draw(HDC hdc)
{
	int saveTicket = SaveDC(hdc);
	
	LOGBRUSH logBrush;

	logBrush.lbStyle = BS_SOLID;
	logBrush.lbColor = RGB(0, 255, 0);
	logBrush.lbHatch = 0;

	HPEN pen = ExtCreatePen(
		PS_SOLID | PS_JOIN_ROUND | PS_GEOMETRIC,
		5,
		&logBrush,
		0,
		NULL);
	SelectObject(hdc, pen);
	
	HGDIOBJ dcBrush = GetStockObject(DC_BRUSH);
	SelectObject(hdc, dcBrush);
	SetDCBrushColor(hdc, RGB(200, 100, 50));

	Rectangle(hdc, 
		rect.left, rect.top,
		rect.right, rect.bottom);

	RestoreDC(hdc, saveTicket);
	DeleteObject(pen);
}
