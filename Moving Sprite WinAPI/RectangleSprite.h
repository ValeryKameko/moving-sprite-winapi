#pragma once

#include "Sprite.h"

class RectangleSprite :
	public Sprite
{
public:
	RectangleSprite(RECT rect);
	RectangleSprite(int left, int top, int right, int bottom);
	~RectangleSprite();

	virtual void Draw(HDC hdc) override;
};

