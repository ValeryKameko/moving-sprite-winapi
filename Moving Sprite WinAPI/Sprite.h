#pragma once

#include <Windows.h>

class Sprite
{
public:
	virtual ~Sprite() = 0;

	virtual void Move(int dx, int dy);
	const RECT &GetRect();

	virtual void Draw(HDC hdc) = 0;
	virtual bool Hit(POINT pt);
	
protected:
	Sprite(RECT rect);
	Sprite(int left, int top, int right, int bottom);

	RECT rect;
};

