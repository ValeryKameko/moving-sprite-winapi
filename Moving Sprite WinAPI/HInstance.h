#pragma once

#include <windows.h>

class HInstance
{
public:
	static HINSTANCE GetHInstance();
	static void SetHInstance(HINSTANCE hInstance);

private:
	HInstance();

	static HINSTANCE hInstance;
};

