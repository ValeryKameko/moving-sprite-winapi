#include <wchar.h>
#include <windowsx.h>
#include "MainWindow.h"
#include "HInstance.h"
#include "RectangleSprite.h"
#include "EllipseSprite.h"
#include "ImageSprite.h"
#include "Sprite.h"


static const wchar_t* MAIN_WINDOW_CLASSNAME = L"Main_Window_Class";
constexpr int SPRITE_STEP = 3;
constexpr int TIME_STEP = 33;
constexpr int IDT_UPDATE_TIMER = 0;
constexpr int MIN_WINDOW_WIDTH = 300;
constexpr int MIN_WINDOW_HEIGHT = 300;
constexpr int WHEEL_SPEED_FACTOR = 4;

MainWindow::MainWindow(HWND hwnd)
{
	sprite = new ImageSprite(0, 0, 100, 100);

	this->hwnd = hwnd;
	SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

	SetTimer(hwnd, IDT_UPDATE_TIMER, TIME_STEP, (TIMERPROC) NULL);
}

MainWindow::~MainWindow()
{
	delete sprite;
}

ATOM MainWindow::RegisterWindowClass(HINSTANCE hInstance)
{
	WNDCLASSEX wndClassEx = {};
	wndClassEx.cbSize = sizeof(wndClassEx);
	wndClassEx.style = 0;
	wndClassEx.lpfnWndProc = MainWindow::WndProc;
	wndClassEx.cbClsExtra = 0;
	wndClassEx.hInstance = hInstance;
	wndClassEx.hIcon = (HICON)NULL;
	wndClassEx.hCursor = (HCURSOR)NULL;// LoadCursor();
	wndClassEx.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wndClassEx.lpszMenuName = NULL;
	wndClassEx.lpszClassName = MAIN_WINDOW_CLASSNAME;
	wndClassEx.hIconSm = (HICON)NULL;

	return RegisterClassEx(&wndClassEx);;
}

MainWindow* MainWindow::CreateNewWindow(LPCWSTR className, HWND parent)
{
	HWND hwnd = CreateWindowEx(
		0,
		MAIN_WINDOW_CLASSNAME,
		className,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		parent, NULL,
		HInstance::GetHInstance(),
		NULL);
	if (!hwnd)
		return NULL;

	return new MainWindow(hwnd);
}

HWND MainWindow::GetHWND()
{
	return hwnd;
}

void MainWindow::ReleaseResources()
{
	KillTimer(hwnd, IDT_UPDATE_TIMER);
}

LRESULT CALLBACK MainWindow::WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	RECT clientRect;
	MainWindow* self = reinterpret_cast<MainWindow*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

	switch (uMsg) {
	case WM_DESTROY:
		self->ReleaseResources();
		PostQuitMessage(0);
		return 0;
	case WM_PAINT:
		return self->OnPaint(uMsg, wParam, lParam);
	case WM_KEYDOWN:
		return self->OnKeyDown(uMsg, wParam, lParam);
	case WM_TIMER:
		return self->OnTimer(uMsg, wParam, lParam);
	case WM_LBUTTONDOWN:
		return self->OnLButtonDown(uMsg, wParam, lParam);
	case WM_MOUSEMOVE:
		return self->OnMouseMove(uMsg, wParam, lParam);
	case WM_MOUSELEAVE:
		return self->OnMouseLeave(uMsg, wParam, lParam);
	case WM_LBUTTONUP:
		return self->OnLButtonUp(uMsg, wParam, lParam);
	case WM_GETMINMAXINFO:
		return self->OnGetMinMaxInfo(uMsg, wParam, lParam);
	case WM_MOUSEWHEEL:
		return self->OnMouseWheel(uMsg, wParam, lParam);
	case WM_SIZING:
	case WM_SIZE:
		if (self)
			self->ProtectBorders();
		GetClientRect(hwnd, &clientRect);
		InvalidateRect(hwnd, &clientRect, false);
	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
}

LRESULT MainWindow::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hwnd, &ps);

	RECT clientRect;
	GetClientRect(hwnd, &clientRect);
	int clientWidth = clientRect.right - clientRect.left;
	int clientHeight = clientRect.bottom - clientRect.top;

	HDC bufferHdc = CreateCompatibleDC(hdc);
	HBITMAP buffer = CreateCompatibleBitmap(hdc, clientWidth, clientHeight);
	HBITMAP oldBuffer = (HBITMAP)SelectObject(bufferHdc, buffer);

	FillRect(bufferHdc, &ps.rcPaint, (HBRUSH)GetStockObject(GRAY_BRUSH));
	this->sprite->Draw(bufferHdc);

	BitBlt(hdc, 0, 0, clientWidth, clientHeight, bufferHdc, 0, 0, SRCCOPY);

	SelectObject(bufferHdc, oldBuffer);

	DeleteObject(buffer);
	DeleteDC(bufferHdc);

	bool result = EndPaint(hwnd, &ps);
	if (!result) {
		MessageBox(NULL, L"Cannot paint in MainWindow", L"Error", MB_OK);
		return -1;
	}
	return 0;
}

LRESULT MainWindow::OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return 0;
}

LRESULT MainWindow::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (wParam) {
	case IDT_UPDATE_TIMER:
		OnUpdateTimer();
		return 0;
	}
	return -1;
}

LRESULT MainWindow::OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	int x = GET_X_LPARAM(lParam);
	int y = GET_Y_LPARAM(lParam);

	lastDraggingPoint.x = x;
	lastDraggingPoint.y = y;

	isDragging = sprite->Hit(lastDraggingPoint);

	return 0;
}

LRESULT MainWindow::OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	TRACKMOUSEEVENT event;
	event.cbSize = sizeof(event);
	event.hwndTrack = hwnd;
	event.dwFlags = TME_LEAVE;

	TrackMouseEvent(&event);

	int x = GET_X_LPARAM(lParam);
	int y = GET_Y_LPARAM(lParam);

	if (isDragging) {
		int dx = x - lastDraggingPoint.x;
		int dy = y - lastDraggingPoint.y;
		sprite->Move(dx, dy);
		ProtectBorders();

		lastDraggingPoint.x = x;
		lastDraggingPoint.y = y;

		RECT clientRect;
		GetClientRect(hwnd, &clientRect);
		InvalidateRect(hwnd, &clientRect, false);
	}
	return 0;
}

LRESULT MainWindow::OnMouseLeave(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	isDragging = false;
	return 0;
}

LRESULT MainWindow::OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	isDragging = false;
	return 0;
}

void MainWindow::OnUpdateTimer()
{
	POINT direction;
	direction.x = direction.y = 0;
	if (!isDragging) {
		if (GetKeyState(VK_DOWN) & 0x8000)
			direction.y += SPRITE_STEP;
		if (GetKeyState(VK_UP) & 0x8000)
			direction.y -= SPRITE_STEP;
		if (GetKeyState(VK_RIGHT) & 0x8000)
			direction.x += SPRITE_STEP;
		if (GetKeyState(VK_LEFT) & 0x8000)
			direction.x -= SPRITE_STEP;
	}

	if (direction.x != 0 || direction.y != 0) {
		sprite->Move(direction.x, direction.y);
		ProtectBorders();

		RECT clientRect;
		GetClientRect(hwnd, &clientRect);
		InvalidateRect(hwnd, &clientRect, false);
	}
}


void MainWindow::ProtectBorders() 
{
	RECT clientRect;
	GetClientRect(hwnd, &clientRect);
	int clientWidth = clientRect.right - clientRect.left;
	int clientHeight = clientRect.bottom - clientRect.top;

	RECT spriteRect = sprite->GetRect();
	if (spriteRect.right > clientWidth)
		sprite->Move(clientWidth - spriteRect.right, 0);
	if (spriteRect.left < 0)
		sprite->Move(-spriteRect.left, 0);
	if (spriteRect.bottom > clientHeight)
		sprite->Move(0, clientHeight - spriteRect.bottom);
	if (spriteRect.top < 0)
		sprite->Move(0, -spriteRect.top);
}


LRESULT MainWindow::OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LPMINMAXINFO pMinMaxInfo = (LPMINMAXINFO) lParam;
	pMinMaxInfo->ptMinTrackSize.x = MIN_WINDOW_WIDTH;
	pMinMaxInfo->ptMinTrackSize.y = MIN_WINDOW_HEIGHT;
	return 0;
}

LRESULT MainWindow::OnMouseWheel(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (!isDragging) {
		int wheelDelta = GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_SPEED_FACTOR;

		if (GET_KEYSTATE_WPARAM(wParam) == MK_SHIFT)
			sprite->Move(wheelDelta, 0);
		else
			sprite->Move(0, wheelDelta);
			
		ProtectBorders();

		RECT clientRect;
		GetClientRect(hwnd, &clientRect);
		InvalidateRect(hwnd, &clientRect, false);
	}

	return 0;
}
