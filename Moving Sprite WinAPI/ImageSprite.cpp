#include "ImageSprite.h"
#include "Wingdi.h"

constexpr int CUT_LEFT = 7;
constexpr int CUT_RIGHT = 7;
constexpr int CUT_BOTTOM = 4;
constexpr int CUT_TOP = 8;

ImageSprite::ImageSprite(RECT rect)
	: ImageSprite(rect.left, rect.top, rect.right, rect.bottom)
{
}

ImageSprite::ImageSprite(int left, int top, int right, int bottom)
	: Sprite(left, top, right, bottom)
{
	DWORD bufferSize = GetCurrentDirectoryW(0, NULL);
	LPWSTR buffer = (LPWSTR)HeapAlloc(GetProcessHeap(), 0, bufferSize);
	GetCurrentDirectoryW(bufferSize, buffer);
	image = LoadImage(NULL, L"sprite256.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	auto err = GetLastError();
	region = CreateRectRgnIndirect(&rect);
}

ImageSprite::~ImageSprite()
{
	DeleteObject(image);
	DeleteObject(region);
}

void ImageSprite::Move(int dx, int dy)
{
	Sprite::Move(dx, dy);
	OffsetRgn(region, dx, dy);
}

void ImageSprite::Draw(HDC hdc)
{
	HDC tempDC = CreateCompatibleDC(hdc);
	HBITMAP oldBitmap = (HBITMAP)SelectObject(tempDC, image);
	BITMAP bitmap;

	int width = rect.right - rect.left;
	int height = rect.top - rect.bottom;

	SelectObject(hdc, image);
	GetObject(image, sizeof(bitmap), &bitmap);
	TransparentBlt(
		hdc,
		rect.left, rect.top,
		bitmap.bmWidth, bitmap.bmHeight,
		tempDC,
		CUT_LEFT, CUT_TOP,
		bitmap.bmWidth - CUT_LEFT - CUT_RIGHT,
		bitmap.bmHeight - CUT_TOP - CUT_BOTTOM,
		RGB(255, 255, 255));

	SelectObject(tempDC, oldBitmap);
	DeleteDC(tempDC);
}

bool ImageSprite::Hit(POINT pt)
{
	return PtInRegion(region, pt.x, pt.y);
}
