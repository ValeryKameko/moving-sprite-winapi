#include "EllipseSprite.h"


EllipseSprite::EllipseSprite(RECT rect)
	: EllipseSprite(rect.left, rect.top, rect.right, rect.bottom)
{
}

EllipseSprite::EllipseSprite(int left, int top, int right, int bottom)
	: Sprite(left, top, right, bottom)
{
	region = CreateEllipticRgnIndirect(&rect);
}

EllipseSprite::~EllipseSprite()
{
	DeleteObject(region);
}

void EllipseSprite::Move(int dx, int dy)
{
	Sprite::Move(dx, dy);
	OffsetRgn(region, dx, dy);
}

void EllipseSprite::Draw(HDC hdc)
{
	int saveTicket = SaveDC(hdc);

	LOGBRUSH logBrush;

	logBrush.lbStyle = BS_SOLID;
	logBrush.lbColor = RGB(0, 255, 0);
	logBrush.lbHatch = 0;

	HPEN pen = ExtCreatePen(
		PS_SOLID | PS_JOIN_ROUND | PS_GEOMETRIC,
		5,
		&logBrush,
		0,
		NULL);
	SelectObject(hdc, pen);

	HGDIOBJ dcBrush = GetStockObject(DC_BRUSH);
	SelectObject(hdc, dcBrush);
	SetDCBrushColor(hdc, RGB(200, 100, 50));

	Ellipse(hdc,
		rect.left, rect.top,
		rect.right, rect.bottom);

	RestoreDC(hdc, saveTicket);
	DeleteObject(pen);
}

bool EllipseSprite::Hit(POINT pt)
{
	return PtInRegion(region, pt.x, pt.y);
}
