﻿#include <windows.h>
#include "HInstance.h"
#include "MainWindow.h"

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR nCmdLine, int nCmdShow)
{
	HInstance::SetHInstance(hInstance);

	if (!MainWindow::RegisterWindowClass(hInstance))
	{
		MessageBox(NULL, L"Cannot create MainWindow class", L"Error", MB_OK);
		return -1;
	}

	MainWindow* window = MainWindow::CreateNewWindow(L"Main Window", NULL);
	if (!window)
	{
		MessageBox(NULL, L"Cannot create MainWindow window", L"Error", MB_OK);
		return -1;
	}

	ShowWindow(window->GetHWND(), nCmdShow);
	UpdateWindow(window->GetHWND());

	MSG msg = {};
	BOOL result;
	while (result = GetMessage(&msg, NULL, 0, 0)) {
		if (result == -1)
		{
			MessageBox(NULL, L"GetMessage error", L"Error", MB_OK);
			return -1;
		}
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}