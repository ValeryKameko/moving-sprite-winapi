#include "Sprite.h"

Sprite::Sprite(RECT rect)
{
	this->rect = rect;
}

Sprite::Sprite(int left, int top, int right, int bottom)
{
	rect.left = left;
	rect.top = top;
	rect.right = right;
	rect.bottom = bottom;
}

Sprite::~Sprite()
{
}

void Sprite::Move(int dx, int dy)
{
	OffsetRect(&rect, dx, dy);
}

const RECT &Sprite::GetRect()
{
	return rect;
}

bool Sprite::Hit(POINT pt)
{
	return PtInRect(&rect, pt);
}
