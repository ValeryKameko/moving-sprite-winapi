#pragma once
#include "Sprite.h"
class EllipseSprite :
	public Sprite
{
public:
	EllipseSprite(RECT rect);
	EllipseSprite(int left, int top, int right, int bottom);

	~EllipseSprite();

	virtual void Move(int dx, int dy) override;
	virtual void Draw(HDC hdc) override;
	virtual bool Hit(POINT pt) override;
private:
	HRGN region;
};

